/* 
   chardevioctl.c - ten program demonstruje użycie poleceń ioctl
	dla kontroli modułu jądra. Takiej funkcjonalności dla własnych
	modułów nie zapewnia żaden program
*/

//Należy załączyć plik nagłówkowy definiujący polecenia ioctl
//#include "chardev.h"
//Alternatywnie należy wpisać definicję ioctl IDENTYCZNĄ jak w module jądra

#include <linux/kernel.h>

#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <linux/fs.h>          //dlta fops załącza się też <linux/mutex.h>
#include <linux/types.h>       //dla typu dev_t
#include <linux/kdev_t.h>      //dla makra MKDEV

// fcntl - dla open, unistd - dla exit, ioctl dla ioctl 
#define MAJOR_NUM 100
#define IOCTL_SET_MSG _IOW( myint,1,int )
#define IOCTL_CHANGE_BUF_SIZE _IOW( MAJOR_NUM, 1, int )

int myint;
void ioctlSetBufSize( int deskr, unsigned rozm ){
	int kodBledu;

	kodBledu = ioctl( deskr, IOCTL_CHANGE_BUF_SIZE, rozm );
	if( kodBledu < 0 ){
		printf("Nieudany IOCTL_CHANGE_BUF_SIZE z błędem nr %d\n", kodBledu );
		exit( -1 );
	}
	printf("Ustawiono nowy rozmiar %d\n", rozm );
  
}

int main(void){
	int deskrPliku, kodBledu;

	// W poniższej instrukcji należy dostosować nazwę pliku urządzenia znakowego
	deskrPliku = open( "/dev/chardev", 0 );
	if( deskrPliku < 0 ){
		printf("Nie można otworzyć pliku urządzenia /dev/chardev\n" );
		exit( -1 );
	}
	printf("deskr pliku %s\n",deskrPliku );
        // Plik otwarty, wywołanie ioctl
 	ioctlSetBufSize( deskrPliku, myint );
	// Zamknięcie pliku
	close( deskrPliku );
        return 0;
}