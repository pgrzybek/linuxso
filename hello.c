#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/fs.h>
#include <asm/uaccess.h>
#include <linux/types.h>
#include <linux/kdev_t.h>
#include <linux/cdev.h>
#include <linux/moduleparam.h>
#include <linux/stat.h>
//#include <linux/slab.h>
#include <linux/fcntl.h>

static int __init chardev_init( void );
static void __exit chardev_exit( void );
static int device_open( struct inode *, struct file * );
static int device_release( struct inode *, struct file * );
static ssize_t device_read( struct file *, char *, size_t, loff_t *);
static ssize_t device_write( struct file *, const char *, size_t, loff_t *);

static int buforSize = 8;
static char *text=1234567890;
static int textLenght=8;

module_param(buforSize, int, S_IRGRP | S_IWGRP );
module_param(textLenght, int, S_IRGRP | S_IWGRP );
module_param(text, charp, 0);

#define SUCCESS 0
#define DEVICE_NAME "bufor"

static int Major =122;
DEFINE_MUTEX( Device_Open );

static char *bufor;
static int end;
static int start;
static int licznik =0;
static struct cdev *chcdev;

static struct file_operations fops ={
	.read = device_read,
	.write = device_write,
	.open = device_open,
	.release = device_release
};


static int __init chardev_init( void ) {

	int i;
	char *tmp;
	tmp = kmalloc (buforSize, GFP_KERNEL);

	if ( textLenght > buforSize ) {
		for (i = 0; i < buforSize; i++) 
			tmp[i]=text[i + textLenght - buforSize];
		end = 0;

	} else {
		for (i = 0; i < buforSize; i++) {
			if ( i < textLenght )
				tmp[i] = text[i];
			else
				tmp[i] = 0; 
		}	
		end = textLenght;
	}

	bufor = kmalloc (buforSize, GFP_KERNEL);
	for (i=0; i < buforSize; i++) 
		bufor[i]=0;

	start=0;
	sprintf( bufor, tmp );
	kfree(tmp);


	int ret;

	dev_t num;
	num = MKDEV( Major, 0);
	ret = register_chrdev_region( num, 3 , DEVICE_NAME );

	if( ret < 0 ){
		printk( KERN_ALERT "Nieudany przydzial urzadzenia. Kod bledu: %d\n", ret );
		return ret;
	}

	chcdev = cdev_alloc();
	chcdev->owner = THIS_MODULE;
	chcdev->ops = &fops;
	ret = cdev_add( chcdev, num, 3 );

	if ( ret < 0 ){
		printk( KERN_ALERT "Nieudany przydzial urzadzenia. Kod bledu: %d\n", ret );
		return ret;
	}

	printk( KERN_INFO "Udany przydzial uradzenia\n");

	return SUCCESS;
}

static void __exit chardev_exit ( void ){

	kfree(bufor);

	dev_t num;
	num = MKDEV( Major, 0 );
	cdev_del( chcdev );
	unregister_chrdev_region( num, 3 );

	printk ( KERN_INFO "Wyrejestrowano modul\n" );
}

static int device_open( struct inode *inoda, struct file *plik) {

	printk( KERN_INFO "Otwarcie pliku o numerze %d \n", iminor(inoda) );

	if ( mutex_lock_interruptible(&Device_Open) ) {
		printk( KERN_INFO "Nieudane przejecie semafora\n" );
		return -ERESTARTSYS;
	}

	try_module_get( THIS_MODULE );
	
	return SUCCESS;
}

static int device_release( struct inode *inoda, struct file *plik ) {

	printk( KERN_INFO "Zamkniecie pliku o numerze %d \n", iminor(inoda) );

	mutex_unlock(&Device_Open);
	module_put( THIS_MODULE );

	return SUCCESS;
}

static ssize_t device_read( struct file *plik, char *buforUz, size_t  dlugosc, loff_t *offset ){

	int odczytano = 0;
	
	printk(KERN_INFO "Rozpoczeto czytanie\n");

	if ( licznik == 0 ) {
		start = end;
		int i;

		for ( i = 0; i < buforSize; i++ ) {

			if ( copy_to_user( buforUz++, (bufor + start), 1) != 0 )	
				return -EFAULT;
			else {
				start++;

				if (start == buforSize) 
					start = 0;
				
				odczytano++;
			}

			licznik++;
		}
	}

	*offset += odczytano;
	printk( KERN_INFO "Zakonczono czytanie\n" );
	printk( KERN_INFO "Ilosc odczytanych danych: %d\n", odczytano );

	return odczytano;
}

static ssize_t device_write( struct file *plik, const char *buforUz, size_t dlugosc, loff_t *offset ){

	int i;
	printk( KERN_INFO "Rozpoczeto zapis\n" );

	if ( !( plik->f_flags & O_APPEND ) ) {
		for ( i = 0; i < buforSize; i++) 
			bufor[i] = 0;

		start = 0;
		end = 0;
	}

	for ( i = 0; i < dlugosc - 1; i++) {

		if ( copy_from_user( (bufor + end), buforUz++, 1) != 0 )	
			return -EFAULT;
		else {
			end++;

			if (end == buforSize) 
				end = 0;
		}
	}

	licznik = 0;
	printk( KERN_INFO "Zakonczono zapis\n" );
	return dlugosc;
}

module_init( chardev_init );
module_exit( chardev_exit );
