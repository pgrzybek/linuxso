//insmod lab2damian.ko myint=10
//rmmod lab2damian.ko
// var/logs (messages file)
//wywolanie : dd if=jakisplik.txt of=/dev/chtdev bs=2
//echo hi>/dev/chardev zapis
//cat /dev/chardev wyswietlanie
//ioctl works
//http://www.linuxquestions.org/questions/linux-kernel-70/error-unknown-field-%27ioctl%27-while-compiling-a-driver-for-kernel-2-6-38-8-a-4175451281/

#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/fs.h>          //dlta fops załącza się też <linux/mutex.h>
#include <asm/uaccess.h>        //dla put_user i get_user
#include <linux/types.h>       //dla typu dev_t
#include <linux/kdev_t.h>      //dla makra MKDEV
#include <linux/cdev.h>        //dla cdev
#include <linux/slab.h>
#include <linux/sched.h>
//prototypy funkcji powinny być w pliku .h

static int __init chardev_init( void );
static void __exit chardev_exit (void );
static int device_open( struct inode *, struct file *);
static int device_release( struct inode *, struct file *);
static ssize_t device_read( struct file *, char *, size_t, loff_t *);
static ssize_t device_write( struct file *, const char *, size_t, loff_t *);
static int change_size(struct inode *inoda, struct file *file, /* ditto */
                       unsigned int ioctl_num, /* number and param for ioctl */
                       unsigned long ioctl_param);
int myint;
#define MAJOR_NUM 5
#define SUCCESS 0
#define DEVICE_NAME "chardev"

//#define BUF_LEN 100
#define IOCTL_CHANGE_BUF_SIZE _IOW( MAJOR_NUM, 1, int );//z pliku ioctl
//#define IOCTL_SET_MSG _IOW( myint,1,int )

/*zmienne globalne są statyczne, z przez to pozostają globalne dla pliku */
static int Major = 122;   //Głowny numer urzadzenia w /dev
DEFINE_MUTEX( Device_Open );   //Mutex zabezpieczający przerd wyścigiem o zasoby
DEFINE_MUTEX(Bufor_Full);
DECLARE_WAIT_QUEUE_HEAD(glowa_kolejki)

//static char msg[ BUF_LEN ];    //Bufor na wiadomość od modułu
static char *msg;
static char *msg_Ptr;          //Wskaźnik do ostatnio odczytanego miejsca w buforze
static int licznik = 0;

static struct cdev *chcdev;

/*  Struktura file_operations jest zdefiniowana w linux/fs.h i zawiera wskaźniki do różnych funkcji,
    które sterownik urządzenia może zdefiniować, a które będą wywoływane w przeróżnych sytuacjach.
    Poniżej zdefiniowano cztery najbardziej podstawowe z nich, korzystając z rozszerzenia zdefiniowanego w stamdardzie c99
*/
struct CircularBuffer
{
    char *bufor;
    char *pocz;
    char *kon;
    size_t wypelnienie;
    char *kursor;
    size_t rozmiar;
} cykliczny;
//static CircularBuffer cykliczny;

static struct file_operations fops =
{
    .read = device_read,
    .write = device_write,
    //.unlocked_ioctl = change_size,
    .open = device_open,
    .release = device_release,
};
//ioctl definition
// int (*ioctl) (struct inode *, struct file *, unsigned int, unsigned long);
module_param(myint, int, S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);
MODULE_PARM_DESC(myint, "An integer");
//Funkceja inicjalizująca moduł powinna mieć zawartość:

static int change_size(struct inode *inoda, struct file *plik, unsigned int ioctl_num, unsigned long ioctl_param)
{
    int deskrPliku;
    if (myint > 0)
    {
        //deskrPliku =device_open( "/dev/chardev", 0 );
        //IOCTL_CHANGE_BUF_SIZE
    }
    return 0;
};

static int __init chardev_init( void )
{
    int ret;
    dev_t num;
    /* W jądrach od 2.60 zmianiłą sie numeracja urządzeń. Typ dev_t jest obecnie liczbą 32bitową, której pierwwsze 12 bitówq
       określa major a reszta minor. Do zbudowania typu dev_t z Major i Minor służy moniższe makro:
    */

    printk("myint %d \n", myint);
    if (myint > 0)
    {
        cykliczny.bufor = kmalloc(myint, GFP_KERNEL);
        if (!cykliczny.bufor)
        {
            return -ENOMEM;
        }
        cykliczny.rozmiar = myint;
        cykliczny.wypelnienie = 0;
        cykliczny.pocz = cykliczny.bufor;
        cykliczny.kon = cykliczny.bufor;
        cykliczny.kursor = cykliczny.bufor;
    }
    else
    {

        printk("Blad wpisz liczbe wieksza niz 0 \n");
        return -EFAULT;
    }

    //koniec naszej roboty

    num = MKDEV( Major, 0 );

    /*Rejestracja  obszaru urządzeń w jądrze. Pierwszym parametrem funkcji jest pierwszy z numerów żadanego obszaru
      (jego część Minor najczęściej jest równa zero, ale nie to wymóg formalny). Drugim jest liczba numerów, które chce się zarejestrować. Trzecim
      jest nazwa. Wartość zwrócona niezerowa oznacza błąd przydziału urządzenia
    */

    ret = register_chrdev_region( num, 3, DEVICE_NAME );
    if ( ret < 0 )
    {
        printk( KERN_ALERT " Nieudana próba przydziału obszaru urządzenia"
                " w jądrze - zwrócony numer %d\n", ret );
        return ret;
    }


    /* Poniższe instrukcje służą do synamicznej alokacji cdev potrzebnej do rejestracji urządzenia znakowego w jądrze > 2.6.0
       oraz do inicjalizaci jej najważniejszych pól */

    chcdev = cdev_alloc();
    chcdev->owner = THIS_MODULE;
    chcdev->ops = &fops;

    /*Rejestracja urządzenia znakowego w jądrze. Pierwszym parametrem jest wskaźnik do struktury cdev, drugim numer
      (w postaci dev_t) początku obszaru, za który odpowiedzialność jest zapisana w cdev, trzecim rozmiar.
      Wartość zwrócona mniejsza od zera oznacza błąd rejestracji. */

    ret = cdev_add( chcdev, num, 3 );
    if ( ret < 0 )
    {
        printk( KERN_ALERT "Nieudana próba zarejestrowania urządzenia"
                " w jądrze - zwrócony numer %d\n", ret );
        return ret;
    }

    //Komunikat do logów systemowych
    printk( KERN_INFO "Przydzielono mi nume urządzenia %d. Otwórz plik\n", Major );
    printk( KERN_INFO "urządzenia za pomocą"
            "'mknod /dev/%s c %d 0', a potem\n", DEVICE_NAME, Major );
    printk( KERN_INFO "z inną ostatnią cyfrą. Próbuj czytać i pisać do tego\n" );
    printk( KERN_INFO "urządzenia. Po usunięciu urządzenia usuń i plik\n" );

    return SUCCESS;
}


// funkcja wyrejestrowująca moduł ma zawartość:

static void __exit chardev_exit( void )
{
    dev_t num;
    /* W jądrach od 2.6.0 zmieniła się numeracja urządzeń. Typ dev_t jest liczbą 32 bitową, w której 12 bitów określa Major, a reszta Minor
    Do zbudowania typu dev_t z Major i Minor służy poniższe makro: */
    num = MKDEV( Major, 0 );
    /* funkcja wyrejestrowująca urządzenie znakowe z jądra ( zwraca void ) */
    cdev_del( chcdev );
    /* funkcja zwalniająca zarezerwowany obszar urządzeń ( zwraca void )*/
    unregister_chrdev_region( num, 3 );
    printk( KERN_INFO "Żegnaj, świecie!\n" );
    kfree(msg);
}

module_init( chardev_init );
module_exit( chardev_exit );

MODULE_LICENSE("GPL v2");
//MODULE_LICENSE("Proprietary");
// Autor modułu
MODULE_AUTHOR( "Dariusz Bismor <Darius.Bismor[at]polsl.pl>" );
//Krótki opis modułu
MODULE_DESCRIPTION( "Przykład modułu z plikiem urządzenia" );
//Modul używa urządzenia /dev/test */
MODULE_SUPPORTED_DEVICE( DEVICE_NAME );


//Kod funkcji otwarcia pliku

static int device_open( struct inode *inoda, struct file *plik )
{
    /*informacja o numerze pobocznym makra unsigned int iminor( struct inode *in) i unsigned int imajor( struct inode *in) zwracjaą numer główny
    i poboczny pliku urządzenia wskazywanego przez inodę in */
    printk( KERN_INFO "Otwarcie pliku urządzniea o numerze pobocznym"
            " %d\n", iminor(inoda) );
    //próba przejęcia semafora
    if ( mutex_lock_interruptible(&Device_Open) )
    {
        printk( KERN_INFO "Próba przejęcia semafora przerwana!\n" );
        /*Zwrócona wartość różna od zera oznacza przerwanie procesu użytkownika. W takim przypadku semafor nie został przejęty */
        return -ERESTARTSYS;
    }

    /*Zwiększenie licznika użycia modułu, uniemożliwi jego usunięcia z jądra */
    try_module_get( THIS_MODULE );
    printk(KERN_INFO "%s \n", msg);
    /* Zapisanie komunikatu, który zostanie przepidsany do pamięci użytkownika w momencie odczytu */
    //sprintf( msg, "hello, world! mówię po raz %d\n", ++licznik );
    //msg_Ptr = msg;
    return SUCCESS;
}

// funkcja zwalniająca urządenia wywoływana przy zamykaniu pliku urządzenia zwraca mutex

static int device_release( struct inode *inoda, struct file *plik )
{
    mutex_unlock(&Device_Open);  //odblokowanie dostepu
    /* jeśli nie zmniejszy się licznika użycia modułu nie da się go usunąć z jądra */
    module_put( THIS_MODULE );
    return 0;
}

// funkcja odczytu urządzenia

static ssize_t device_read( struct file *plik, char *buforUz, size_t wypelnienie, loff_t *offset )
{
    int przeczytano ;
    //mutex_try_lock(&Bufor_Full)
    if (mutex_lock_interruptible(&Bufor_Full))
    {
        return -ERESTARTSYS;
    };

    while (wypelnienie > 0)
    {
        mutex_unlock(&Bufor_Full)
        if (wait_event_interruptible(glowa_kolejki, wypelnienie == 0))
        {
            return -ERESTARTSYS;
        };
        if (mutex_lock_interruptible(&Bufor_Full))
        {
            return -ERESTARTSYS;
        };
    };
    printk(" wypelnienie %d\n", wypelnienie );

    if (cykliczny.wypelnienie == 0)
    {
        return 0;
    }
    printk(" wypelnienie %d\n", wypelnienie );
    cykliczny.kursor = cykliczny.pocz;
    if (cykliczny.wypelnienie < cykliczny.rozmiar)
    {
        /* code */
        while (wypelnienie > 0 && cykliczny.kursor < cykliczny.kon)
        {
            put_user(*(cykliczny.kursor++), buforUz++);
            --wypelnienie;
            ++przeczytano;
        }
    }
    else
    {
        do
        {
            put_user(*(cykliczny.kursor++), buforUz++);
            --wypelnienie;
            ++przeczytano;
            if (cykliczny.kursor - cykliczny.bufor == cykliczny.rozmiar)
            {
                cykliczny.kursor = cykliczny.bufor;
            }
        }
        while (wypelnienie > 0 && cykliczny.kursor != cykliczny.kon);
    }
    *offset += przeczytano;
    mutex_unlock(&Bufor_Full);
    wake_up_interruptible(&glowa_kolejki);
    return przeczytano;
}

//jeśli osiągnięto koniec bufora zwracamy 0
//koniec jest rozpoznany bo jest w nim znak końca łańcucha znakowego \0
//if( *msg_Ptr == 0 ){
//return 0;
//}

//Przepisanie danych do bufora użytkownika
//while( wypelnienie && *msg_Ptr ){
/*ponieważ bufor jest w przestrzeni danych użytkonika a nie w pamięci jądra, nie mozna danych przepisywać bezpośrednio
a jedynie za popmocą funkcji put_user która służy do przepisywania danych z pamięci jądra do pamięci użytkownika */
//put_user( *(msg_Ptr++), buforUz++ );
//wypelnienie--;
//odczytano++;
//}
//*offset +=przeczytano;
//wiekszość funkcji do odczytu zwraca ilość przepisanych danych
//return przeczytano;
//}
//Kod funkcji device_write()

static ssize_t device_write( struct file *plik, const char *buforUz, size_t wypelnienie, loff_t *offset )
{
    if (mutex_lock_interruptible(&Bufor_Full))
    {
        return -ERESTARTSYS;
    }

    while (wypelnienie == 0)
    {
        mutex_unlock(&Bufor_Full)
        if (wait_event_interruptible(glowa_kolejki, wypelnienie < 0))
        {
            return -ERESTARTSYS;
        }
        if (mutex_lock_interruptible(&Bufor_Full))
        {
            return -ERESTARTSYS;
        }
    }

    int zapisano = 0;
    if (wypelnienie == 0)
    {
        return 0;
    }
    cykliczny.kursor = cykliczny.kon;
    while (wypelnienie > 0)
    {
        get_user(*(cykliczny.kursor++), buforUz++);
        if (cykliczny.kursor - cykliczny.bufor == cykliczny.rozmiar)
        {
            cykliczny.kursor = cykliczny.bufor;
        }
        if (cykliczny.wypelnienie < cykliczny.rozmiar)
        {
            ++cykliczny.wypelnienie;
        }
    }
    cykliczny.kon = cykliczny.kursor;
    if (cykliczny.wypelnienie == cykliczny.rozmiar)
    {
        cykliczny.pocz = cykliczny.kursor;
    }
    *offset += zapisano;
    mutex_unlock(&Bufor_Full);
    wake_up_interruptible(&glowa_kolejki);
    return zapisano;
}