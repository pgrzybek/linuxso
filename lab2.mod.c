#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
 .name = KBUILD_MODNAME,
 .init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
 .exit = cleanup_module,
#endif
 .arch = MODULE_ARCH_INIT,
};

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0xbc257400, "module_layout" },
	{ 0x7485e15e, "unregister_chrdev_region" },
	{ 0xad3e5417, "cdev_del" },
	{ 0xa316da0c, "cdev_add" },
	{ 0x1934270b, "cdev_alloc" },
	{ 0xd8e484f0, "register_chrdev_region" },
	{ 0xd6367922, "kmem_cache_alloc_trace" },
	{ 0xa98b8bdc, "kmalloc_caches" },
	{ 0x37a0cba, "kfree" },
	{ 0x362ef408, "_copy_from_user" },
	{ 0x2e60bace, "memcpy" },
	{ 0x12da5bb2, "__kmalloc" },
	{ 0x2f287f0d, "copy_to_user" },
	{ 0x62cd8754, "__tracepoint_module_get" },
	{ 0xf155d448, "mutex_lock_interruptible" },
	{ 0x50eedeb8, "printk" },
	{ 0x2a6c3c71, "module_put" },
	{ 0x39fa5441, "mutex_unlock" },
	{ 0xb4390f9a, "mcount" },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=";


MODULE_INFO(srcversion, "43AD5E9FFA025466D65C9D8");
