/*By skomplilowac stworz plik o nazwie Makefile o tresci

obj-m += lab1base.o

all:
	make -C /lib/modules/$(shell uname -r)/build M=$(PWD) modules
clean:
	make -C /lib/modules/$(shell uname -r)/build M=$(PWD) clean

Uwaga na wciecia
By uruchomic
przejdz na admina poleceniem:
sudo su
dodaj do jadra poleceniem: 
insmod lab1base.ko
usun poleceniem
rmmod lab1base.ko

Wyniki sa w
var/log 
wyswietlanie
mknod /dev/chardev c 122 0
cat /dev/chardev
wyswietlanie modulow 
lsmod
usuniecie tego modulu
rmmod lab1base.ko
wyswietla wywolania systemowe
strace cat /dev/chardev
1 sytemowy descryptor
3 descryptor systemu

strace dd if=/dev/chardev bs=5
zapis
echo hi>/dev/chardev
*/
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/fs.h>          //dla fops za³¹cza siê te¿ <linux/mutex.h>
#include <asm/uaccess.h>        //dla put_user i get_user
#include <linux/types.h>       //dla typu dev_t
#include <linux/kdev_t.h>      //dla makra MKDEV
#include <linux/cdev.h>        //dla cdev
#include <linux/slab.h>

//prototypy funkcji powinny byæ w pliku .h

static int __init chardev_init( void );
static void __exit chardev_exit (void );
static int device_open( struct inode *, struct file *);
static int device_release( struct inode *, struct file *);
static ssize_t device_read( struct file *, char *, size_t, loff_t *);
static ssize_t device_write( struct file *, const char *, size_t, loff_t *);

#define SUCCESS 0
#define DEVICE_NAME "chardev"
#define BUF_LEN 100

/*zmienne globalne sa statyczne, z przez to pozostaja globalne dla pliku */
static int Major = 122;   //Glowny numer urzadzenia w /dev
DEFINE_MUTEX( Device_Open );   //Mutex zabezpieczajacy przerd wyacigiem o zasoby
static char msg[ BUF_LEN ];    //Bufor na wiadomosc od modu³u
static char *msg_Ptr;          //Wskaznik do ostatnio odczytanego miejsca w buforze
static int licznik = 0;
static int dlodczytu=0;//przechowuje ilosc znakow odczytanych
static int dlzapisu=0;//przechowuje ilosc znakow zapisanych
static struct cdev *chcdev;
static int readI=0;
static int writeI=0;

/*  Struktura file_operations jest zdefiniowana w linux/fs.h i zawiera wskaniki do ró¿nych funkcji,
    które sterownik urzadzenia moze zdefiniowaæ, a które bêda wywolywane w przeró¿nych sytuacjach.
    Ponizej zdefiniowano cztery najbardziej podstawowe z nich, korzystajac z rozszerzenia zdefiniowanego w stamdardzie c99
*/

static struct file_operations fops =
{
    .read = device_read ,
    .write = device_write ,
    .open = device_open ,
    .release = device_release
};

//Funkcja inicjalizujaca modul powinna mieæ zawartosc:
static int __init chardev_init( void )
{
    int ret;
    dev_t num;
    /* W jadrach od 2.60 zmianila sie numeracja urzadzeñ. Typ dev_t jest obecnie liczba 32bitow¹, której pierwwsze 12 bitówq
       okresla major a reszta minor. Do zbudowania typu dev_t z Major i Minor sluzy monizsze makro:
    */

    num = MKDEV( Major, 0 );

    /*Rejestracja  obszaru urzadzeñ w jadrze. Pierwszym parametrem funkcji jest pierwszy z numerów ¿adanego obszaru
      (jego czêsc Minor najczêsciej jest równa zero, ale nie to wymóg formalny). Drugim jest liczba numerów, które chce siê zarejestrowaæ. Trzecim
      jest nazwa. Wartosc zwrócona niezerowa oznacza b³¹d przydzia³u urz¹dzenia
    */

    ret = register_chrdev_region( num, 3, DEVICE_NAME );
    if ( ret < 0 )
    {
        printk( KERN_ALERT " Nieudana próba przydzialu obszaru urzadzenia"
                " w jadrze - zwrócony numer %d\n", ret );
        return ret;
    }

    /* Ponizsze instrukcje sluza do synamicznej alokacji cdev potrzebnej do rejestracji urz¹dzenia znakowego w j¹drze > 2.6.0
       oraz do inicjalizaci jej najwa¿niejszych pól */

    chcdev = cdev_alloc();
    chcdev->owner = THIS_MODULE;
    chcdev->ops = &fops;

    /*Rejestracja urz¹dzenia znakowego w j¹drze. Pierwszym parametrem jest wskaznik do struktury cdev, drugim numer
      (w postaci dev_t) poczatku obszaru, za który odpowiedzialnosc jest zapisana w cdev, trzecim rozmiar.
      Wartosc zwrócona mniejsza od zera oznacza b³¹d rejestracji. */

    ret = cdev_add( chcdev, num, 3 );
    if ( ret < 0 )
    {
        printk( KERN_ALERT "Nieudana próba zarejestrowania urzadzenia"
                " w jadrze - zwrócony numer %d\n", ret );
        return ret;
    }

    //Komunikat do logów systemowych
    printk( KERN_INFO "Przydzielono mi numer urzadzenia %d. Otwórz plik\n", Major );
    printk( KERN_INFO "urzadzenia za pomoca"
            "'mknod /dev/%s c %d 0', a potem\n", DEVICE_NAME, Major );
    printk( KERN_INFO "z inna ostatnia cyfra. Próbuj czytac i pisac do tego\n" );
    printk( KERN_INFO "urzadzenia. Po usunieciu urzadzenia usun i plik\n" );

    return SUCCESS;
}

// funkcja wyrejestrowuj¹ca modu³ ma zawartoæ:

static void __exit chardev_exit( void )
{
    dev_t num;
    /* W jadrach od 2.6.0 zmienia sie numeracja urzadzeñ. Typ dev_t jest liczba 32 bitow¹, w której 12 bitów okrela Major, a reszta Minor
    Do zbudowania typu dev_t z Major i Minor sluzy ponizsze makro: */

    num = MKDEV( Major, 0 );
    /* funkcja wyrejestrowujaca urzadzenie znakowe z jadra ( zwraca void ) */
    cdev_del( chcdev );
    /* funkcja zwalniajaca zarezerwowany obszar urzadzeñ ( zwraca void )*/
    unregister_chrdev_region( num, 3 );
    printk( KERN_INFO "zegnaj, swiecie!\n" );
}

module_init( chardev_init );
module_exit( chardev_exit );

MODULE_LICENSE("GPL v2");
//MODULE_LICENSE("Proprietary");
// Autor modulu
MODULE_AUTHOR( "Dariusz Bismor <Darius.Bismor[at]polsl.pl>" );
//Krótki opis modulu
MODULE_DESCRIPTION( "Przyklad modulu z plikiem urzadzenia" );
//Modul uzywa urzadzenia /dev/test */
MODULE_SUPPORTED_DEVICE( DEVICE_NAME );


//Kod funkcji otwarcia pliku

static int device_open( struct inode *inoda, struct file *plik )
{
    /*informacja o numerze pobocznym makra unsigned int iminor( struct inode *in) i unsigned int imajor( struct inode *in) zwracja¹ numer g³ówny
    i poboczny pliku urz¹dzenia wskazywanego przez inodê in */
    printk( KERN_INFO "Otwarcie pliku urzadzniea o numerze pobocznym"
            " %d\n", iminor(inoda) );
    //próba przejêcia semafora
    if ( mutex_lock_interruptible(&Device_Open) )
    {
        printk( KERN_INFO "Próba przejecia semafora przerwana!\n" );
        /*Zwrócona wartosc rózna od zera oznacza przerwanie procesu uzytkownika. W takim przypadku semafor nie zosta³ przejêty */
        return -ERESTARTSYS;
    }

    /*Zwiêkszenie licznika uzycia modulu, uniemozliwi jego usuniecia z jadra */
    try_module_get( THIS_MODULE );

    /* Zapisanie komunikatu, który zostanie przepidsany do pamiêci uzytkownika w momencie odczytu */
    //sprint f dodaje znaki \0 pelny zapis to "hello, world! mówie po raz 2\n\0"
    dlodczytu=sprintf( msg, "Hello, world! mówie po raz %d\n", ++licznik );
    msg_Ptr = msg;//ustawia waskaznik na poczatek msg
    return SUCCESS;
}
// funkcja zwalniaj¹ca urz¹denia wywo³ywana przy zamykaniu pliku urz¹dzenia zwraca mutex

static int device_release( struct inode *inoda, struct file *plik )
{
    mutex_unlock(&Device_Open);  //odblokowanie dostepu
    /* jesli nie zmniejszy sie licznika uzycia modulu nie da sie go usunac z jadra */
    module_put( THIS_MODULE );
    return 0;
}
// funkcja odczytu urzadzenia

static ssize_t device_read( struct file *plik, char *buforUz, size_t dlugosc, loff_t *offset )
{
    //buforUz to miejsce gdzie nalezy wpisac odpowiedz, ma on dlugosc miejsca
    //liczba bajtów tym razem wpisanych do bufora - calosc wiadomosci niekoniecznie musi byc przeczytana za jednym razem
    int odczytano;//odczytane dane
    int ret2;//zwracana wartosc
    readI++;
    printk(KERN_INFO "o ilosc wejsc %d",readI);
    if (dlugosc > dlodczytu)//jesli bufor jest wiekszy tekst do odczytu
    {
        odczytano = dlodczytu;//odczytaj calosc ale nie wiecej
    }
    else
    {
        odczytano = dlugosc;//inaczej tylko rozmiar bufora(tyle ile mozna)
    }
    ret2 = copy_to_user(buforUz, msg + *offset, odczytano);
    if (ret2 > 0)
    {
        odczytano = odczytano - ret2;
    }
    
    dlodczytu -= odczytano;
    *offset += odczytano;
    printk( KERN_INFO "Zakonczono czytanie\n" );
    printk(KERN_INFO "dlodczytu %d\n", dlodczytu);
	printk( KERN_INFO "Ilosc odczytanych danych: %d\n", odczytano );
    //wiekszosc funkcji do odczytu zwraca ilosc przepisanych danych
    return odczytano;
}

//Kod funkcji device_write()

static ssize_t device_write( struct file *plik, const char *bufor, size_t dlugosc, loff_t *offset )
{
  	int ret;//wartosc zwacana przez copy_from_user
    int zapisano;
    zapisano = dlugosc;
    writeI++;
    printk(KERN_INFO "z ilosc wejsc  %d",writeI);
    
    if (dlugosc > dlzapisu)//jesli bufor jest wiekszy tekst do odczytu
    {
        zapisano = dlzapisu;//odczytaj calosc ale nie wiecej
    }
    else
    {
        zapisano = dlugosc;//inaczej tylko rozmiar bufora(tyle ile mozna)
    }
    
    ret = copy_from_user(msg+*offset, bufor, zapisano);
    if (ret > 0)
    {
        zapisano = zapisano - ret;
    }
    
    dlzapisu -= zapisano;
    *offset += zapisano;
    printk(KERN_INFO "dlodczytu %d\n", dlzapisu);
	printk( KERN_INFO "Ilosc zapisanych danych: %d\n", zapisano );
    printk( KERN_INFO "Zakonczono czytanie\n" );
    return zapisano;
    //printk( KERN_ALERT "To urzadzenie nie obsluguje zapisu!\n" );
    //return -EINVAL;
}