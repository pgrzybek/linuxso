#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/fs.h>
#include <asm/uaccess.h>
#include <linux/types.h>
#include <linux/kdev_t.h>
#include <linux/cdev.h>
#include <linux/slab.h>

static int __init chardev_init(void);
static void __exit chardev_exit(void);
static int device_open(struct inode *, struct file *);
static int device_release(struct inode *, struct file *);
static ssize_t device_read(struct file *, char *, size_t, loff_t *);
static ssize_t device_write(struct file *, const char *, size_t, loff_t *);

#define SUCCESS 0
#define DEVICE_NAME "chardev"
#define BUF_LEN 100

static int Major = 122;
DEFINE_MUTEX(Device_Open);
static char *msg;
static char *msg_Ptr;
static int licznik = 0;
static struct cdev *chcdev;

static struct file_operations fops =
{
    .read = device_read,
    .write = device_write,
    .open = device_open,
    .release = device_release
};
static int __init chardev_init(void)
{
    int ret;
    dev_t num;
    msg = kmalloc(BUF_LEN, GFP_KERNEL);
    if (!msg)
    {
        return -ENOMEM;
    }
    num = MKDEV( Major, 0);
    ret = register_chrdev_region(num, 3, DEVICE_NAME);
    if (ret < 0)
    {
        printk(KERN_ALERT "Nieudana prĂłba przydziaÂłu obszaru urzÂądzenia w jÂądzrze - zwrĂłcony numer %d\n", ret);
        return ret;
    }
    chcdev = cdev_alloc();
    chcdev->owner = THIS_MODULE;
    chcdev->ops = &fops;
    ret = cdev_add(chcdev, num, 3);
    if (ret < 0)
    {
        printk(KERN_ALERT "Nieudana proba zarejestrowania urzadzneia w jÂądrze - zwrĂłcony numer %d\n", ret );
        return ret;
    }
    printk(KERN_INFO "przydzielono mi numer urzadzenia %d. Otworz plik\n", Major);
    printk(KERN_INFO "urzadzenia za pomoca 'mknod /dev/%s c %d 0' a potem\n", DEVICE_NAME, Major);
    printk(KERN_INFO "z inna ostatnia cyfra. Probuj czytac i pisac do tego \n");
    printk(KERN_INFO "urzadzenia. Po usuniaeciu urzadzenia usun plik\n");
    return SUCCESS;
}

static void __exit chardev_exit(void)
{
    dev_t num;
    num = MKDEV(Major, 0);
    cdev_del(chcdev);
    unregister_chrdev_region(num, 3);
    printk(KERN_INFO "Zegnaj, swiecie!\n");
    kfree(msg);
}

module_init(chardev_init);
module_exit(chardev_exit);
MODULE_LICENSE("GPL v2");
MODULE_AUTHOR ("pgrzybek");
MODULE_DESCRIPTION( "przykladowy modul z plikiem urzadzenia");
MODULE_SUPPORTED_DEVICE(DEVICE_NAME);
static int device_open(struct inode *inoda, struct file *plik)
{
    printk(KERN_INFO "otwarcie pliku urzadzenia o numerze pobocznym %d\n", iminor(inoda));
    if ( mutex_lock_interruptible(&Device_Open))
    {
        printk(KERN_INFO "PrĂłba przejecia semafora przerwana!\n");
        return -ERESTARTSYS;
    }
    try_module_get(THIS_MODULE);
    printk(KERN_INFO "%s \n", msg);
    return SUCCESS;
}
static int device_release(struct inode *inoda, struct file *plik)
{
    mutex_unlock(&Device_Open);
    module_put(THIS_MODULE);
    return 0;
}
static ssize_t device_read(struct file *plik, char *buforUz, size_t dlugosc, loff_t *offset)
{
    int odczytano;
    int ret2;
    if (dlugosc > licznik)
    {
        odczytano = licznik;
    }
    else
    {
        odczytano = dlugosc;
    }
    ret2 = copy_to_user(buforUz, msg + *offset, odczytano);
    if (ret2 < 0)
    {
        return ret2;
    }
    odczytano -= ret2;
    *offset += odczytano;
    licznik -= odczytano;
    return odczytano;
}
static ssize_t device_write(struct file *plik, const char *bufor, size_t dlugosc, loff_t *offset)
{
    int ret;
    int zapisano;
    zapisano = dlugosc;
    msg_Ptr = kmalloc(*offset + dlugosc, GFP_KERNEL);
    if (!msg_Ptr)
    {
        return -ENOSPC;
        kfree(msg_Ptr);
    }
    memcpy(msg_Ptr, msg, *offset);
    ret = copy_from_user(msg_Ptr + *offset, bufor, zapisano);
    if (ret > 0)
    {
        zapisano = zapisano - ret;
    }
    kfree(msg);
    msg = msg_Ptr;
    msg_Ptr = NULL;
    kfree(msg_Ptr);
    zapisano -= ret;
    *offset += zapisano;
    licznik += zapisano;
    return dlugosc;
}